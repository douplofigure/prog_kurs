#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void solve(double a, double b, double c) {

  double delta = b*b - 4 * a * c;

  if (delta > 0) {
    
    printf("x1 = %lf\n", (-b - sqrt(delta))/(2.0*a));
    printf("x2 = %lf\n", (-b + sqrt(delta))/(2.0*a));
		  
  } else if (delta < 0) {

    printf("z1 = %lf + i %lf\n", -b, (sqrt(-delta))/(2.0*a));
    printf("z2 = %lf - i %lf\n", -b, (sqrt(-delta))/(2.0*a));
    
  } else {

    printf("x0 = %lf\n", -b/(2.0*a));

  }

}

int main(int argc, char ** argv) {
  
  //Unsere drei werte
  double a,b,c;
  
  //Hier lese ich die Werte ein
  printf("a=?\n");
  scanf("%lf", &a);
  printf("b=?\n");
  scanf("%lf", &b);
  printf("c=?\n");
  scanf("%lf", &c);
  
  //solve wird aufgerufen
  solve(a,b,c);
  
}
