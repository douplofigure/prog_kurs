#include <stdio.h>
#include <stdlib.h>

/// Funtion um das Spielfeld auszugeben
void printBoard(int * board) {

  printf("-------\n");
  for(int i = 0; i < 3; ++i) {

    for (int j = 0; j < 3; ++j) {
      if (board[i*3+j] == 0) {
	
	printf("|%d", i*3+j+1);
	
      }
      else if (board[i*3+j] == 1) {

	printf("|X");
	
      }
      else {

	printf("|O");
	
      }
    }

    printf("|\n");

  }

  printf("-------\n");

}

/// Funktion um nach der Naechsten Zelle zu fragen.
int getNextCell(int * board) {

  int cell;

  printf("Welche Zelle darf es sein (1-9)?\n");
  scanf("%d", &cell);
  while((cell < 1 || cell > 9) || board[cell-1]) {
    printf("Bitte Zahl zwischen 1 und 9 angeben.\n");
    scanf("%d", &cell);
  }

  return cell;
  
}

/// Funktion um zu zaehlen wie viele Felder noch frei sind.
int countEmptySpots(int * b) {

  int c = 0;
  for (int i = 0; i < 9; ++i)
    if (!b[i])
      c++;
  return c;

}

int hasWon(int * board) {

  for (int i = 0; i < 3; ++i) {

    //Zeilen
    if (board[i*3] && board[i*3] == board[i*3+1] && board[i*3] == board[i*3+2]) return board[i*3];
    //Spalten
    if (board[i] && board[i] == board[i+3] && board[i] == board[i+6]) return board[i];
    
  }

  // Diagonale links-oben rechts-unten
  if (board[0] && board[0] == board[4] && board[0] == board[8]) return board[0];
  // Diagonale rechts-oben links-unten
  if (board[2] && board[2] == board[4] && board[2] == board[6]) return board[2];

}

int main(int argc, char ** argv) {
	
  int board[9]; // Wir brauchen ein Feld mit 9 Kaesten.

  // Wir setzen das feld komplett leer.
  for (int i = 0; i < 9; ++i) {
    
    board[i] = 0;
    
  }

  int currentPlayer = 1; //Der aktuelle Spieler ist Spieler 1
  
  do {
    
    printBoard(board);                  //Wir geben das Spielfeld an.
    int cell = getNextCell(board);      //wir fragen nach der Zelle die belegt werden soll.
    
    board[cell-1] = currentPlayer;      //wir Setzen die Zell auf den aktuellen Spieler.

    int winner = hasWon(board);
    if (winner) {
      printf("%c winns!!!\n", (winner == 1) ? 'X' : 'O');
      break;
    }
    
    currentPlayer = 3 - currentPlayer;  //Wir setzen den Spieler auf 1 falls er 2 ist sonst 2.
    
  } while (countEmptySpots(board) > 0); //wir wiedeholen solange noch Platz ist.

  printBoard(board);                    // Wir zeigen das Ergebniss an.
	
}
